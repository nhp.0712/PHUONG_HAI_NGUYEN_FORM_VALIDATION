1.  Create 1 page named index.html.
2.  Create a form that consists of the following fields:  first name,  last name,  username,  password,  confirm password, e-mail address, confirm e-mail address, phone number, street address, city, state, postal code,
and date of birth. All fields are required.
3.  Write JavaScript to perform the following validation:
•  All fields are required and must be filled in for successful form submission.  This must be checked
using JavaScript.
•  All  fields  must  have  valid  values.   Validity  of  values  must  be  checked  using  regular  expressions  in
JavaScript.
•  All errors must be displayed either beside the field that has caused the error or at the top of the page.
•  Correcting and error must clear the error from display.
•  Username must be 5-12 characters
•  Passwords must have reasonable complexity (5-12 characters, ≥ 1 uppercase, ≥ 1 lowercase, ≥ 1 nu- meric, ≥ 1 symbol)
•  Valid phone number formats include:
a)  (XXX) XXX-XXXX
b)  XXX-XXX-XXXX
c)  XXX XXX XXXX
•  Valid US Postal Codes include;
a)  XXXXX
b)  XXXXX-XXXX
•  Valid email addresses include:
a)  greenr@bgsu.edu
b)  green.r@bgsu.edu
c)  green.robert+charles@bgsu.edu
d)  green+robert@bgsu.edu